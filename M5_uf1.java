import java.util.Scanner;

    /**
     * M5_p4 class represents a simple program with various operations.
     */
    public class M5_p4 {
        private final Scanner sc;

        /**
         * Constructor for M5_p4 class, initializes the Scanner for user input.
         */
        public M5_p4() {
            this.sc = new Scanner(System.in);
        }

        /**
         * Reads an integer from the user input.
         *
         * @return The integer entered by the user.
         */
        public int readUserInput() {
            return sc.nextInt();
        }

        /**
         * Displays a farewell message.
         */
        public void end() {
            System.out.println("FI");
        }

        /**
         * Checks if a given string is a palindrome and prints the result.
         */
        public void palindrom() {
            String text = this.sc.nextLine();

            boolean palindrom = true;

            for (int i = 0; i < text.length() / 2 && palindrom; i++) {
                if (text.charAt(i) != text.charAt(text.length() - i - 1)) {
                    palindrom = false;
                    break;
                }
            }

            System.out.println(palindrom);
        }

        /**
         * Counts the number of words and letters in a given string and prints the results.
         */
        public void wordCount() {
            String text = this.sc.nextLine();
            int wordsCount = text.split("\\s+").length;
            int lettersCount = text.replaceAll("[^a-zA-Z]", "").length();
            System.out.println("Letters: " + wordsCount);
            System.out.println("Words: " + lettersCount);
        }

        /**
         * Displays the menu options for the program.
         */
        public void showMenu() {
            System.out.println("Operacions:");
            System.out.print("Escull una opció:");
            System.out.println(" ");
            System.out.println("0.Sortir del programa");
            System.out.println("4.Word count");
            System.out.println("6.Palindrom");
        }

        /**
         * Performs the operation based on the user's choice.
         *
         * @param opcio The user's choice.
         */
        public void operate(int opcio) {
            switch (opcio) {
                case 0:
                    end();
                    break;
                case 4:
                    wordCount();
                    break;
                case 6:
                    palindrom();
                    break;
            }
        }

        /**
         * The main method that creates an instance of M5_p4 and runs the program.
         *
         * @param args Command line arguments (not used in this program).
         */
        public static void main(String[] args) {
            M5_p4 p4 = new M5_p4();
            int response;
            do {
                p4.showMenu();
                response = p4.readUserInput();
                p4.operate(response);
            } while (response != 0);
        }
    }